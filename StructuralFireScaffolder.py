# This is a scaffold script that can add the following predefind items to the model:
#  - Adds an isotropic thermal concrete material based on EC2
#  - Adds an isotropic thermal steel material based on EC3
#  - Adds an isotropic thermal wood material based EC5
#  - Adds the amplitude for the standard fire (ISO834)
#  - Adds convection and radiation interactions for concrete, steel and timber

from abaqus import mdb
from abaqusConstants import *
import regionToolset
import math
import numpy as np


def StructuralFireScaffold(txtModelName,
                           chkEC2Concrete, fltConcreteAmbientDensity, fltConcreteMoistureContent,
                           chkEC3Steel,
                           chkEC5Timber, fltTimberAmbientDensity, fltTimberMoistureContent,
                           chkISOStdFire,
                           chkInteractionConcrete, chkInteractionSteel, chkInteractionTimber):
    """
    Add the scaffolding items to the abaqus model based on the user inputs
    """
    try:
        # current_model_name = session.sessionState[session.currentViewportName]['modelName']
        current_model = mdb.models[txtModelName]
    except Exception:
        raise Exception('Model with name "{' + txtModelName + '}" does not exist')

    ### Model Properties ###
    if current_model.absoluteZero is None:
        current_model.setValues(absoluteZero=-273)
    if current_model.stefanBoltzmann is None:
        current_model.setValues(stefanBoltzmann=5.67e-8)

    ### MATERIAL MODELS ###
    if chkEC2Concrete:
        add_EC2_Concrete_Thermal(current_model, fltConcreteAmbientDensity, fltConcreteMoistureContent/100.0)
    if chkEC3Steel:
        add_EC3_Steel_Thermal(current_model)
    if chkEC5Timber:
        add_EC5_Timber_Thermal(current_model, fltTimberAmbientDensity, fltTimberMoistureContent/100.0)

    ### STANDARD FIRE AMPLITUDE ###
    if chkISOStdFire:
        add_iso_std_fire(current_model)

    ### INTERACTIONS ###
    if chkInteractionConcrete or chkInteractionSteel or chkInteractionTimber:

        # get dimensionality of current assembly (if any, if none then assume 3D)
        _instances = current_model.rootAssembly.instances.values()
        model_dimensionality = '3D'
        if len(_instances) > 0:
            _dimensionality = _instances[0].part.queryGeometry(printResults=False)['space']
            if _dimensionality == THREE_D:
                model_dimensionality = '3D'
            elif _dimensionality == TWO_D_PLANAR:
                model_dimensionality = '2D'
            else:
                print('Unknown model dimensionality, using 3D')

        # based on dimensionality make a dummy surface
        if model_dimensionality == '3D':
            surface_region, dummy_part, dummy_instance = __make_dummy_surface_3d(current_model)
        elif model_dimensionality == '2D':
            surface_region, dummy_part, dummy_instance = __make_dummy_surface_2d(current_model)

        # Check that first step is heat transfer capable, so that the interactions can be added to it
        # If only the initial step exists then try create a heat transfer step to put the interactions initial
        if len(current_model.steps.values()) == 1:
            try:
                step = current_model.HeatTransferStep(name='Fire', previous='Initial', maxNumInc=100000, deltmx=5.0)
                step_name = step.name
            except Exception as e:
                raise Exception('Failed to create heat transfer step' + e)
        else:
            # check that first step is heat transfer capacble
            _first_step = current_model.steps.values()[1]
            if _first_step.procedureType == HEAT_TRANSFER or _first_step.procedureType == COUPLED_TEMP_DISPLACEMENT:
                step_name = _first_step.name

        if chkISOStdFire:
            convection_coefficient = 25
            amplitude_name = 'ISO834StandardFire'
        else:
            convection_coefficient = 35
            amplitude_name = ''

        if chkInteractionConcrete:
            add_convection_interaction(current_model, step_name, surface_region, 'Concrete-Convection', convection_coefficient, sink_amplitude=amplitude_name)
            add_radiation_interaction(current_model, step_name, surface_region, 'Concrete-Radiation', 0.7, ambient_amplitude=amplitude_name)

        if chkInteractionSteel:
            add_convection_interaction(current_model, step_name, surface_region, 'Steel-Convection', convection_coefficient, sink_amplitude=amplitude_name)
            add_radiation_interaction(current_model, step_name, surface_region, 'Steel-Radiation', 0.7, ambient_amplitude=amplitude_name)

        if chkInteractionTimber:
            add_convection_interaction(current_model, step_name, surface_region, 'Timber-Convection', convection_coefficient, sink_amplitude=amplitude_name)
            add_radiation_interaction(current_model, step_name, surface_region, 'Timber-Radiation', 0.8, ambient_amplitude=amplitude_name)

        # remove dummy part and instances
        __cleanup_dummy(current_model, dummy_part, dummy_instance)

    return


def __EC2_density(temp, density_20):
    """
    The density (kg/m^3) according to EC2 cl 3.3.2 at the specified temperature.
    density_20 is the density of the concrete at 20 degrees C.
    """

    if temp <= 115:
        return density_20
    elif temp <= 200:
        return density_20 * (1 - 0.02 * (temp - 115)/85)
    elif temp <= 400:
        return density_20 * (0.98 - 0.03 * (temp - 200)/200)
    elif temp <= 1200:
        return density_20 * (0.95 - 0.07 * (temp - 400)/800)

    return None


def __EC2_conductivity(temp):
    """
    The thermal conductivity (W/mK) according to EC2 cl 3.3.3 (2) (the lower limit) at the specified temperature.
    The UK National Annex says the thermal conductivity of concrete should be taken as the lower limit give in the eurocode.
    cl 3.3.3 of EC2 says this relationship is for normal weight concrete.
    """
    return 1.36 - 0.136 * (temp/100) + 0.0057 * (temp/100)**2


def __EC2_specific_heat(temp, moisture_content):
    """
    The specific heat (J/kgK) according to EC2 cl 3.3.2(2) at the specific temperature
    moisture_content is percentage oisture content in concrete (2% is 0.02)
    """

    # manually interpolate to get c_p_peak using EC2 cl 3.3.2(2)
    # manual interpolation is required because MC - c_p_peak relationshipo is slightly bilinear
    c_p_peak = 900
    if moisture_content <= 0.015:
        c_p_peak = 900 + 570 * (moisture_content/0.015)
    elif moisture_content <= 0.03:
        c_p_peak = 1470 + 550 * ((moisture_content-0.015)/0.015)
    else:
        raise Exception("Moisture content too great")

    if temp < 100:
        return 900
    elif temp <= 115:
        return c_p_peak
    elif temp <= 200:
        return c_p_peak - ((c_p_peak - 1000)/85) * (temp - 115)
    elif temp <= 400:
        return 1000 + (temp - 200)/2
    elif temp <= 1200:
        return 1100

    return None


def add_EC2_Concrete_Thermal(model, density_20, moisture_content):
    """
    EC2 (1992-1-2) Concrete Material Model
    density_20 is the density (kg/m^3) of the concrete at 20 degrees
    moisture_content is the moisture content of the conrete (2% is 0.02)

    Density (kg/m^3) from cl 3.3.2
    Thermal Conductivity (W/mK) using the lower limit (specified in the UK national annex). This clause states it is for normal weight concrete.
    Specific Heat (J/kgK) from cl 3.3.2(2) where moisture content is accounted for with a "peak" in the specific heat
    """

    ConcreteMaterial = model.Material(name='Concrete-EC2-Thermal')
    ConcreteMaterial.setValues(description='Concrete thermal material model from Eurocode 2')

    temps = [20, 50, 100, 115] + list(range(150, 1201, 50))
    density_table = []
    for temp in temps:
        density_table.append((__EC2_density(temp, density_20), temp))
    ConcreteMaterial.Density(temperatureDependency=ON, table=density_table)

    temps = [20] + range(50, 1201, 50)
    conductivity_table = []
    for temp in temps:
        conductivity_table.append((__EC2_conductivity(temp), temp))
    ConcreteMaterial.Conductivity(temperatureDependency=ON, table=conductivity_table)

    temps = [20, 50, 99.9, 100, 115, 150] + range(200, 1201, 50)
    specific_heat_table = []
    for temp in temps:
        specific_heat_table.append((__EC2_specific_heat(temp, moisture_content), temp))
    ConcreteMaterial.SpecificHeat(temperatureDependency=ON,
                                  law=CONSTANTPRESSURE,
                                  table=specific_heat_table)

    model.HomogeneousSolidSection(name='Concrete-EC2-Thermal', material='Concrete-EC2-Thermal', thickness=None)

    print("Added EC2 Concrete Material")
    return


def __EC3_conductivity(temp):
    """
    The thermal conductivity (W/mK) according to EC3 cl 3.4.1.3 at the specified temperature
    """
    if temp < 800:
        return 54 - 3.33e-2 * temp
    elif temp <= 1200:
        return 27.3

    return None


def __EC3_specific_heat(temp):
    """
    The specific heat (J/kgK) according to EC3 cl 3.4.1.2 at the specific temperature
    """
    if temp < 600:
        return 425 + 0.773 * temp + 1.69e-3 * temp**2 + 2.22e-6 * temp**3
    elif temp < 735:
        return 666 + 13002.0 / (738 - temp)
    elif temp < 900:
        return 545 + 17820.0 / (temp - 731)
    elif temp <= 1200:
        return 650

    return None


def _add_EC3_steel_thermal_properties(steel_material):
    """
    EC3 (1993-1-2) Steel Material Model
    Density is 7850 kh/m^3 from cl 3.2.2
    Thermal Conductivity (W/mK) from clause 3.4.1.3
    Specific Heat (J/kgK) from cl 3.4.1.2
    """

    steel_material.Density(table=((7850.0, ), ))

    temps = [20] + list(range(50, 1201, 50))
    conductivity_table = []
    for temp in temps:
        conductivity_table.append((__EC3_conductivity(temp), temp))
    steel_material.Conductivity(temperatureDependency=ON, table=conductivity_table)

    temps = [20] + range(50, 600, 50) + range(600, 735, 5) + range(735, 900, 5) + range(900, 1201, 50)
    specific_heat_table = []
    for temp in temps:
        specific_heat_table.append((__EC3_specific_heat(temp), temp))
    steel_material.SpecificHeat(temperatureDependency=ON,
                               law=CONSTANTPRESSURE,
                               table=specific_heat_table)

    return


def __EC3_elastic_modulus_at_temp(ambient_E, temperature):
    """
    Returns the elastic modulus of a mild steel at the given temperature according to Table 3.1 EC3 (1993-1-2)
    """
    temps = [20, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200]
    E_temp = [1.0, 1.0, 0.9, 0.8, 0.7, 0.6, 0.31, 0.13, 0.09, 0.0675, 0.045, 0.0225, 0.0]

    return ambient_E * np.interp(temperature, temps, E_temp)


def __EC3__proportional_limit_at_temp(ambient_fy, temperature):
    """
    Returns the proportional limit of a mild steel at the given temperature according to Table 3.1 EC3 (1993-1-2)
    """
    temps = [20, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200]
    proportional_limit_temp = [1.0, 1.0, 0.807, 0.613, 0.42, 0.36, 0.18, 0.075, 0.05, 0.0375, 0.025, 0.0125, 0.0]

    return ambient_fy * np.interp(temperature, temps, proportional_limit_temp)


def __EC3_eff_yield_strength(ambient_fy, temperature):
    """
    Returns the effective yield strength of a mild steel at the given temperature according to Table 3.1 EC3 (1993-1-2)
    """
    temps = [20, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200]
    eff_fy_temp = [1.0, 1.0, 1.0, 1.0, 1.0, 0.78, 0.47, 0.23, 0.11, 0.06, 0.04, 0.02, 0.0]

    return ambient_fy * np.interp(temperature, temps, eff_fy_temp)

def __EC3_stress_plastic_strain(temperature, ambient_fy, ambient_E):
    """
    Calculates the stress-strain curve for a mild steel with anbient fy and E at the given temperature
    according to EC3 (1993-1-2) Figure 3.1 & Table 3.1
    """

    yield_strain = 0.02
    limiting_strain_for_yield_strength = 0.15
    ultimate_strain = 0.2

    elastic_modulus_temperature = __EC3_elastic_modulus_at_temp(ambient_E, temperature)
    proportional_limit_stress = __EC3__proportional_limit_at_temp(ambient_fy, temperature)
    effective_yield_strength = __EC3_eff_yield_strength(ambient_fy, temperature)

    proportional_limit_strain = proportional_limit_stress / elastic_modulus_temperature

    _plastic_strain = []
    _stress = []
    for plastic_strain in np.linspace(0.0, yield_strain - proportional_limit_strain, 10):
        strain = proportional_limit_strain + plastic_strain
        c = ((effective_yield_strength - proportional_limit_stress)**2) / ((yield_strain - proportional_limit_strain)*elastic_modulus_temperature - 2*(effective_yield_strength - proportional_limit_stress))
        b = c * (yield_strain - proportional_limit_strain) * elastic_modulus_temperature + c**2
        a = sqrt((yield_strain - proportional_limit_strain)*(yield_strain - proportional_limit_strain + c/elastic_modulus_temperature))
        _stress.append(proportional_limit_stress - c + (b/a)*sqrt(a**2 - (yield_strain - strain)**2))
        _plastic_strain.append(plastic_strain)

    for __strain in np.linspace(0.0, limiting_strain_for_yield_strength - yield_strain, 10):
        _stress.append(effective_yield_strength)
        _plastic_strain.append(yield_strain + __strain)

    for __strain in np.linspace(0.0, ultimate_strain - limiting_strain_for_yield_strength, 10):
        _stress.append(effective_yield_strength * (1 - __strain/(ultimate_strain - limiting_strain_for_yield_strength)))
        _plastic_strain.append(limiting_strain_for_yield_strength + __strain)

    return _stress, _plastic_strain


def _add_EC3_steel_mechanical_properties(steel_material, ambient_elastic_modulus, ambient_yield_strength):
    # TODO

    temps_to_evaluate = [20] + range(100, 1200, 100)

    poissons_ratio = 0.3  # from EN 1993-1-1 CL 3.2.6

    elastic_data = []
    plastic_data = []
    for temp in temps_to_evaluate:
        elastic_data.append((__EC3_elastic_modulus_at_temp(ambient_elastic_modulus, temp), poissons_ratio, temp))
        _stress, _plastic_strain = __EC3_stress_plastic_strain(temp, ambient_yield_strength, ambient_elastic_modulus)
        for i in range(len(_stress)):
            plastic_data.append((_stress[i], _plastic_strain[i], temp))

    steel_material.Elastic(temperatureDependency=ON,
                          table=lastic_data)
    steel_material.Plastic(temperatureDependency=ON,
                          scaleStress=None, table=plastic_data)
    return


def add_EC3_Steel_Thermal(model):
    """
    Create a meterial model for steel with thermal properties as specified in EC3 (1993-1-2)
    """

    SteelMaterial = model.Material(name='Steel-EC3-Thermal')
    SteelMaterial.setValues(description='Steel thermal material model from Eurocode 3')

    _add_EC3_steel_thermal_properties(SteelMaterial)

    model.HomogeneousSolidSection(name='Steel-EC3-Thermal', material='Steel-EC3-Thermal', thickness=None)

    print("Added EC3 Steel Material")
    return


def add_EC3_Steel_Mechanical_Thermal(model, ambient_elastic_modulus, ambient_yield_strength):
    """
    Create a meterial model for steel with mechanical and thermal properties as specified in EC3 (1993-1-2)
    """

    SteelMaterial = model.Material(name='Steel-EC3-MechnicalThermal')
    SteelMaterial.setValues(description='Steel mechnical and thermal material model from Eurocode 3')

    _add_EC3_steel_thermal_properties(SteelMaterial)
    _add_EC3_steel_mechanical_properties(SteelMaterial, ambient_elastic_modulus, ambient_yield_strength)

    model.HomogeneousSolidSection(name='Steel-EC3-Mechnical Thermal', material='Steel-EC3-Thermal', thickness=None)

    print("Added EC3 Steel Material")
    return


def add_EC5_Timber_Thermal(model, density, moisture_content):
    """
    EC5 (1995-1-2) Timber Material Model
    moisture_content is the percentage moisture content at ambient (12% is 0.12)
    density is the density (kg/m^3) of the timber at ambient (at the specified moisture content)

    Density (kg/m^3) from table B2
    Thermal Conductivity (W/mK) from table B1
    Specific Heat (J/kgK) from table B2
    The spike in specific heat as the bound water is heated toi a gas is implemented usin gthe latent heat functionality in Abaqus.
    This results in a faster computation (avoiding difficult numerical convergence) at this spike.
    """
    TimberMaterial = model.Material(name='Timber-EC5-Thermal')
    TimberMaterial.setValues(description='Timber thermal material model from Eurocode 5')

    # density of zero moisture content timber
    density_0 = density / (1.0 + moisture_content)

    TimberMaterial.Density(temperatureDependency=ON,
                           table=(((1.0+moisture_content)*density_0, 20.0),
                                  ((1.0+moisture_content)*density_0, 99.0),
                                  (1.0*density_0, 120.0),
                                  (1.0*density_0, 200.0),
                                  (0.93*density_0, 250.0),
                                  (0.76*density_0, 300.0),
                                  (0.52*density_0, 350.0),
                                  (0.38*density_0, 400.0),
                                  (0.28*density_0, 600.0),
                                  (0.26*density_0, 800.0),
                                  (1e-5*density_0, 1200.0)))
    # Density can't go to zero otherwise abaqus gives error, so make it very small instead

    TimberMaterial.Conductivity(temperatureDependency=ON,
                                table=((0.12, 20.0),
                                       (0.15, 200.0),
                                       (0.07, 350.0),
                                       (0.09, 500.0),
                                       (0.35, 800.0),
                                       (1.5, 1200.0)))

    TimberMaterial.SpecificHeat(temperatureDependency=ON,
                                law=CONSTANTPRESSURE,
                                table=((1530.0, 20.0),
                                       (1770.0, 99.0),
                                       (2120.0, 120.0),
                                       (2000.0, 200.0),
                                       (1620.0, 250.0),
                                       (710.0, 300.0),
                                       (850.0, 350.0),
                                       (1000.0, 400.0),
                                       (1400.0, 600.0),
                                       (1650.0, 800.0),
                                       (1650.0, 1200.0)))

    # heat of vaporisation is back calculated from the EC5 table B2 values where these are for 12% MC
    # for these values the latent heat (energy more than a linear relationship from 99 deg C to 120 deg C in specific heat)
    # is 243.705 kJ/kg. This works out to a heat of vaporization for the water of 2031 kJ/kg per % moisture content.
    TimberMaterial.LatentHeat(table=((2031e3*moisture_content, 99.0, 120.0), ))

    model.HomogeneousSolidSection(name='Timber-EC5-Thermal', material='Timber-EC5-THermal', thickness=None)

    print("Added EC5 Timber Material")
    return


def __iso834Temp(time):
    """
    Calculates temperature in deg C for a given time in seconds in the ISO834 Standard Fire
    """
    t = time / 60.0  # get time in minutes
    return 20 + 345*math.log10(8*t+1)


def add_iso_std_fire(model, step=30, max_time=120):
    """
    Add the iso std fire as an amplitude (units deg C)
    step is disctretisation step in seconds (int).
    max_time is duration of fire in minutes (int).
    Note that for the first five minutes the standard fire is discretised in 10 second steps
    """

    data = []
    if max_time < 5:
        max_time = 5
    times = range(0, 5*60, 10) + range(5*60, (max_time+1)*60, step)
    for t in times:
        data.append((t, __iso834Temp(t)))

    model.TabularAmplitude(name='ISO834StandardFire', timeSpan=STEP, smooth=SOLVER_DEFAULT, data=tuple(data))

    print("Added ISO834 Standard Fire Amplitude")
    return


def __make_dummy_surface_3d(model):
    """
    Adds a dummy surface (in 3d model space) to the model for adding interactions to the model.
    Returns the newly added dummy surface.
    """
    s = model.ConstrainedSketch(name='dummy_sketch', sheetSize=2.0)
    s.rectangle(point1=(0.0, 0.0), point2=(1.0, 1.0))
    part = model.Part(name='dummy_shell',
                   dimensionality=THREE_D,
                   type=DEFORMABLE_BODY)
    part.BaseShell(sketch=s)
    del model.sketches[s.name]  # delete the sketch from the sketches part of the model
    a = model.rootAssembly
    csys = a.DatumCsysByDefault(CARTESIAN)  # make a cartesian csys if not already present
    instance = a.Instance(name='dummy_shell-1', part=part, dependent=ON)
    del a.features[csys.name]  # delete csys as no longer needed

    # get a face region to assign interactions to
    s1 = instance.faces
    side1Faces1 = s1.getSequenceFromMask(mask=('[#1 ]', ), )
    region = regionToolset.Region(side1Faces=side1Faces1)

    return region, part, instance


def __make_dummy_surface_2d(model):
    """
    Adds a dummy surface (in 3d model space) to the model for adding interactions to the model.
    Returns the newly added dummy surface.
    """

    s = model.ConstrainedSketch(name='dummy_sketch', sheetSize=2.0)
    s.rectangle(point1=(0.0, 0.0), point2=(1.0, 1.0))
    part = model.Part(name='dummy_shell',
                   dimensionality=TWO_D_PLANAR,
                   type=DEFORMABLE_BODY)
    part.BaseShell(sketch=s)
    del model.sketches[s.name]  # delete the sketch from the sketches part of the model
    a = model.rootAssembly

    csys = a.DatumCsysByDefault(CARTESIAN)  # make a cartesian csys if not already present
    instance = a.Instance(name='dummy_shell-1', part=part, dependent=ON)
    del a.features[csys.name]  # delete csys as no longer needed

    # get a face region to assign interactions to
    s1 = instance.edges
    side1Edges1 = s1.getSequenceFromMask(mask=('[#8 ]', ), )
    region = regionToolset.Region(side1Edges=side1Edges1)

    return region, part, instance


def __cleanup_dummy(model, part, instance):
    """
    Delete the specified part and instance from tyhe model
    """

    # delete the dummy instance form the assembly
    a = model.rootAssembly
    del a.features[instance.name]

    # delete the dummy part
    del model.parts[part.name]

    return


def add_convection_interaction(model, step_name, surface_region, interaction_name, film_coefficient, sink_amplitude=''):
    """
    Add a surface film interaction to the step with step_name for the surface_region with the film_coefficient and the sink amplitude of sink_amplitude (leave sink_amplitude blank for no amplitude), for the specified model.
    The interaction is given the name interaction_name.
    """

    model.FilmCondition(name=interaction_name,
                        createStepName=step_name,
                        surface=surface_region, definition=EMBEDDED_COEFF,
                        filmCoeff=film_coefficient, filmCoeffAmplitude='',
                        sinkTemperature=1.0, sinkAmplitude=sink_amplitude,
                        sinkDistributionType=UNIFORM, sinkFieldName='')

    return


def add_radiation_interaction(model, step_name, surface_region, interaction_name, emissivity, ambient_amplitude=''):
    """
    Add a surface radiation interaction to the step with step_name for the surface_region with the emissivityd the ambient amplitude of ambient_amplitude (leave ambient_amplitude blank for no amplitude), for the specified model.
    The interaction is given the name interaction_name.
    """

    model.RadiationToAmbient(name=interaction_name, createStepName=step_name,
                             surface=surface_region,
                             radiationType=AMBIENT, distributionType=UNIFORM, field='',
                             emissivity=emissivity, ambientTemperature=1.0, ambientTemperatureAmp=ambient_amplitude)

    return
